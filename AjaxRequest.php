<?php

namespace sycomponent;

use Yii;
use yii\base\Widget;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class AjaxRequest extends Widget {

    public $modelClass = '';
    public $createUrl = ['create'];
    public $createButtonText = 'Tambah Data';

    private $jscript = '';

    public function init() {
        parent::init();

        $this->jscript .= '
            var ajaxRequest = function(thisObj, method) {

                var type = "GET";

                if (method != undefined) {
                    type = method;
                }

                $.ajax({
                    cache: false,
                    type: type,
                    url: thisObj.attr("href"),
                    beforeSend: function(xhr) {
                        $(".overlay").show();
                        $(".loading-img").show();
                    },
                    success: function(response) {

                        $("#main-content").html(response);

                        $("html, body").animate({scrollTop: 0}, "slow");

                        $(".overlay").hide();
                        $(".loading-img").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);

                        $(".overlay").hide();
                        $(".loading-img").hide();

                        $(".error-overlay").find(".error-number").html(xhr.status);
                        $(".error-overlay").find(".error-message").html(xhr.responseText);
                        $(".error-overlay").show();

                        setTimeout(function() {
                            $(".error-overlay").hide();
                        }, 1000);
                    }
                });
            };
        ';
    }

    public function index() {

        $this->jscript .= '
            $("#grid-view-' . Inflector::camel2id(StringHelper::basename($this->modelClass)) . '").find("a").on("click", function() {

                if ($(this).hasClass("direct")) {
                    return true;
                }

                if ($(this).attr("data-not-ajax") == undefined) {
                    ajaxRequest($(this), $(this).attr("data-method"));
                }

                return false;
            });

            $("#grid-view-' . Inflector::camel2id(StringHelper::basename($this->modelClass)) . '").find("input, select").on("change keydown", function(event) {

                if (event.type === "keydown") {
                    if (event.keyCode !== 13) {
                        return true;
                    }
                }

                var query = "";

                $("#grid-view-' . Inflector::camel2id(StringHelper::basename($this->modelClass)) . '").find("input, select").each(function() {
                    query += $(this).attr("name") + "=" + $(this).val() + "&";
                });

                var endUrl = $("#refresh").attr("href");
                if (endUrl.indexOf("?") >= 0) {
                    endUrl = "&";
                } else {
                    endUrl = "?";
                }

                $.ajax({
                    cache: false,
                    url: $("#refresh").attr("href") + endUrl + query,
                    beforeSend: function(xhr) {
                        $(".overlay").show();
                        $(".loading-img").show();
                    },
                    success: function(response) {

                        $("#main-content").html(response);

                        $("html, body").animate({scrollTop: 0}, "slow");

                        $(".overlay").hide();
                        $(".loading-img").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr);

                        $(".overlay").hide();
                        $(".loading-img").hide();

                        $(".error-overlay").find(".error-number").html(xhr.status);
                        $(".error-overlay").find(".error-message").html(xhr.responseText);
                        $(".error-overlay").show();

                        setTimeout(function() {
                            $(".error-overlay").hide();
                        }, 1000);
                    }
                });

                return false;
            });

            var buttonCreate = $("#button-create").children().clone();

            buttonCreate.on("click", function() {

                ajaxRequest($(this));

                return false;
            });

            var breadcrumb = $("#breadcrumb").children().clone();

            breadcrumb.find("a").on("click", function() {

                ajaxRequest($(this));

                return false;
            });

            $("#title-page").find("h3").html("");
            $("#title-page").find("h3").append($("#title").html()).append("&nbsp;&nbsp;&nbsp;").append(buttonCreate);

            $("#breadcrumb-page").html("");
            $("#breadcrumb-page").append(breadcrumb);
        ';

        $this->getView()->registerJs($this->jscript);
    }

    public function view() {

        $this->jscript .= '
            $(".' . Inflector::camel2id(StringHelper::basename($this->modelClass)) . '-view").find("a").on("click", function() {

                if ($(this).hasClass("direct")) {
                    return true;
                }

                if ($(this).attr("data-not-ajax") == undefined) {

                    ajaxRequest($(this), $(this).attr("data-method"));
                }

                return false;
            });

            var breadcrumb = $("#breadcrumb").children().clone();

            breadcrumb.find("a").on("click", function() {

                ajaxRequest($(this));

                return false;
            });

            $("#title-page").find("h3").html("");
            $("#title-page").find("h3").append($("#title").html());

            $("#breadcrumb-page").html("");
            $("#breadcrumb-page").append(breadcrumb);
        ';

        $this->getView()->registerJs($this->jscript);
    }

    public function form($isAjax = true) {

        if ($isAjax) {

            $this->jscript .= '
                $("form#' . Inflector::camel2id(StringHelper::basename($this->modelClass)) . '-form").on("beforeSubmit", function(event) {

                    var thisObj = $(this);

                    if(thisObj.find(".has-error").length)  {
                        return false;
                    }

                    var formData = new FormData(this);

                    var endUrl = thisObj.attr("action");
                    if (endUrl.indexOf("?") >= 0) {
                        endUrl = "&";
                    } else {
                        endUrl = "?";
                    }

                    $.ajax({
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: "POST",
                        data: formData,
                        url: thisObj.attr("action") + endUrl + "save=1",
                        beforeSend: function(xhr) {
                            $(".overlay").show();
                            $(".loading-img").show();
                        },
                        success: function(response) {

                            $("#main-content").html(response);

                            $("html, body").animate({scrollTop: 0}, "slow");

                            $(".overlay").hide();
                            $(".loading-img").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr);

                            $(".overlay").hide();
                            $(".loading-img").hide();

                            $(".error-overlay").find(".error-number").html(xhr.status);
                            $(".error-overlay").find(".error-message").html(xhr.responseText);
                            $(".error-overlay").show();

                            setTimeout(function() {
                                $(".error-overlay").hide();
                            }, 1000);
                        }
                    });

                    return false;
                });';
        }

        $this->jscript .= '
            $(".' . Inflector::camel2id(StringHelper::basename($this->modelClass)) . '-form").find("a").on("click", function() {

                if (!$(this).hasClass("direct")) {

                    ajaxRequest($(this), $(this).attr("data-method"));

                    return false;
                }
            });

            var breadcrumb = $("#breadcrumb").children().clone();

            breadcrumb.find("a").on("click", function() {

                ajaxRequest($(this));

                return false;
            });

            $("#title-page").find("h3").html("");
            $("#title-page").find("h3").append($("#title").html());

            $("#breadcrumb-page").html("");
            $("#breadcrumb-page").append(breadcrumb);
        ';

        $this->getView()->registerJs($this->jscript);
    }

    public function component($createNew = false) {

        return $this->render('ajax-request/component', [
            'title' => $this->getView()->title,
            'createUrl' => $this->createUrl,
            'breadcrumbs' => isset($this->getView()->params['breadcrumbs']) ? $this->getView()->params['breadcrumbs'] : [],
            'createNew' => $createNew,
            'createButtonText' => $this->createButtonText,
        ]);
    }

    public static function redirect($controller, $url) {

        $jscript = '
            $.ajax({
                cache: false,
                url: "' . $url .'",
                beforeSend: function(xhr) {
                    $(".overlay").show();
                    $(".loading-img").show();
                },
                success: function(response) {

                    $("#main-content").html(response);

                    $("html, body").animate({scrollTop: 0}, "slow");

                    $(".overlay").hide();
                    $(".loading-img").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);

                    $(".overlay").hide();
                    $(".loading-img").hide();

                    $(".error-overlay").find(".error-number").html(xhr.status);
                    $(".error-overlay").find(".error-message").html(xhr.responseText);
                    $(".error-overlay").show();

                    setTimeout(function() {
                        $(".error-overlay").hide();
                    }, 1000);
                }
            });
        ';

        $controller->getView()->registerJs($jscript, \yii\web\View::POS_HEAD);
        return $controller->render('@sycomponent/views/ajax-request/zero');
    }
}
