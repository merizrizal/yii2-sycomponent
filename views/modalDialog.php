<div class="modal fade" id="modalDeleteConfirmation" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Data</h4>
            </div>
            <div class="modal-body">
                <p>
                    Anda yakin menghapus data ini?
                </p>
            </div><!-- /.box-body -->
            <div class="modal-footer">
                <a id="aYes" href="" class="btn btn-success" data-dismiss="modal">
                    <i class="fa fa-check"></i>
                    Yes
                </a>
                <a class="btn btn-default" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    No
                </a>
            </div>
        </div>
    </div>
</div>

