<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs; ?>

<div id="title" class="hide">
    <?= $title ?>
</div>

<div id="breadcrumb" class="hide">
    <div>
        <?= Breadcrumbs::widget([
            'homeLink' => !empty(Yii::$app->controller->module->id) ? ['label' => Yii::$app->controller->module->name] : ['label' => '', 'template' => ''],
            'links' => $breadcrumbs,
        ]) ?>
    </div>
</div>

<?php
if ($createNew): ?>

    <div id="button-create" class="hide">
        <?= Html::a('<i class="fa fa-upload"></i>&nbsp;&nbsp;&nbsp;' . $createButtonText, $createUrl, ['id' => 'create-new', 'class' => 'btn btn-success']) ?>
    </div>

<?php
endif;

$jscript = '
    $(window).unbind("popstate");
    $(window).bind("popstate", function() {
        $.ajax({
            cache: false,
            url: location.href,
            beforeSend: function(xhr) {
                $(".overlay").show();
                $(".loading-img").show();
            },
            success: function(response) {

                $("#main-content").html(response);

                $(document).scrollTop(0);
                $(".overlay").hide();
                $(".loading-img").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr);

                $(".overlay").hide();
                $(".loading-img").hide();

                $(".error-overlay").find(".error-number").html(xhr.status);
                $(".error-overlay").find(".error-message").html(xhr.responseText);
                $(".error-overlay").show();

                setTimeout(function() {
                    $(".error-overlay").hide();
                }, 1000);
            }
        });
    });

    $(document).prop("title", $("meta[name=\'app\']").attr("content") + " - " + $("#title").html());
    window.history.pushState("string", $(document).prop("title"), "' . Yii::$app->request->url .'");
';

$this->registerJs($jscript); ?>