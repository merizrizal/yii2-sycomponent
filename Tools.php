<?php

namespace sycomponent;

use Yii;
use yii\imagine\Image;
use yii\web\UploadedFile;

class Tools {

    public static function array_sort($array, $on, $order = SORT_ASC) {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    public static function convertToCurrency($value, $isConvert = true) {
        if ($isConvert)
            return Yii::$app->formatter->asCurrency($value);
        else
            return $value;
    }

    public static function uploadFile($basePath, $model, $field, $fieldId, $suffix = '', $isCompress = false) {
        $name = null;

        $file = UploadedFile::getInstance($model, $field);
        if ($file) {

            if (!empty($model->oldAttributes[$field])) {
                $filename = Yii::getAlias('@uploads') . $basePath . $model->oldAttributes[$field];

                if (file_exists($filename))
                    unlink($filename);
            }

            $rand = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 16);

            $flag = $file->saveAs(Yii::getAlias('@uploads') . $basePath . $model->$fieldId . $rand . $suffix . '.' . $file->extension);

            if ($flag) {
                if ($isCompress) {

                    Image::resize(Yii::getAlias('@uploads') . $basePath . $model->$fieldId . $rand . $suffix . '.' . $file->extension, 1921, 1281)
                            ->save(Yii::getAlias('@uploads') . $basePath . $model->$fieldId . $rand . $suffix . '.' . $file->extension , ['quality' => 50]);
                }

                $name = $model->$fieldId . $rand . $suffix . '.' . $file->extension;
            }
        }

        return $name;
    }

    public static function uploadFiles($basePath, $model, $field, $fieldId, $suffix = '', $isCompress = false) {
        $name = [];

        $files = UploadedFile::getInstances($model, $field);
        if ($files) {

            foreach ($files as $file) {

                $rand = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 16);

                $flag = $file->saveAs(Yii::getAlias('@uploads') . $basePath . $model->$fieldId . $rand . $suffix . '.' . $file->extension);

                if ($flag) {
                    if ($isCompress) {

                        Image::resize(Yii::getAlias('@uploads') . $basePath . $model->$fieldId . $rand . $suffix . '.' . $file->extension, 1921, 1281)
                                ->save(Yii::getAlias('@uploads') . $basePath . $model->$fieldId . $rand . $suffix . '.' . $file->extension , ['quality' => 50]);
                    }

                    $name[] = $model->$fieldId . $rand . $suffix . '.' . $file->extension;
                }
            }
        }

        return $name;
    }

    public static function uploadFileWithoutModel($basePath, $formName, $fieldId, $suffix = '', $isCompress = false) {
        $name = null;

        $file = UploadedFile::getInstanceByName($formName);
        if ($file) {

            $rand = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 16);

            $flag = $file->saveAs(Yii::getAlias('@uploads') . $basePath . $fieldId . $rand . $suffix . '.' . $file->extension);

            if ($flag) {
                if ($isCompress) {

                    Image::resize(Yii::getAlias('@uploads') . $basePath . $fieldId . $rand . $suffix . '.' . $file->extension, 1921, 1281)
                            ->save(Yii::getAlias('@uploads') . $basePath . $fieldId . $rand . $suffix . '.' . $file->extension , ['quality' => 50]);
                }

                $name = $fieldId . $rand . $suffix . '.' . $file->extension;
            }
        }

        return $name;
    }

    public static function uploadFilesWithoutModel($basePath, $formName, $fieldId, $suffix = '', $isCompress = false) {
        $name = [];

        $files = UploadedFile::getInstancesByName($formName);
        if ($files) {

            foreach ($files as $file) {

                $rand = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 16);

                $flag = $file->saveAs(Yii::getAlias('@uploads') . $basePath . $fieldId . $rand . $suffix . '.' . $file->extension);

                if ($flag) {
                    if ($isCompress) {

                        Image::resize(Yii::getAlias('@uploads') . $basePath . $fieldId . $rand . $suffix . '.' . $file->extension, 1921, 1281)
                                ->save(Yii::getAlias('@uploads') . $basePath . $fieldId . $rand . $suffix . '.' . $file->extension , ['quality' => 50]);
                    }

                    $name[] = $fieldId . $rand . $suffix . '.' . $file->extension;
                }
            }
        }

        return $name;
    }

    public static function thumb($basePath, $field, $width, $height, $keepRatio = false, $compress = true) {

        if (!empty($field)) {

            $filename = Yii::getAlias('@uploads') . $basePath . $width . 'x' . $height . ($keepRatio ? 'ratio' : '') . $field;

            if (!file_exists($filename)) {

                try {
                    if (!$keepRatio) {

                        Image::thumbnail('@uploads' . $basePath . $field, $width, $height)
                                ->save($filename, ['quality' => $compress ? 50 : 100]);
                    } else {

                        Image::resize('@uploads' . $basePath . $field, $width, $height)
                                ->save($filename , ['quality' => $compress ? 50 : 100]);
                    }

                } catch (\Exception $exc) {

                }
            }

            return $basePath . $width . 'x' . $height . ($keepRatio ? 'ratio' : '') . $field;
        }
    }
}

