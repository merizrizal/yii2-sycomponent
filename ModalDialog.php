<?php

namespace sycomponent;

use yii\base\Widget;

class ModalDialog extends Widget {

    public $clickedComponent = '';
    public $modelAttributeId = '';
    public $modelAttributeName = '';

    public function getScript($index = true) {

        $jscript = '';

        if ($index) {

            $jscript .= '
                $("body").on("click", "' . $this->clickedComponent . '", function(e) {
                    e.preventDefault();
                    var aButton = $(this);
                    $("#modalDeleteConfirmation").on("show.bs.modal", function (e) {
                            var modal = $(this);
                            modal.find(".modal-title").text("Delete Data: (" + aButton.attr("' . $this->modelAttributeId . '") + ") " + aButton.attr("' . $this->modelAttributeName . '"));
                            modal.find("a#aYes").attr("href", aButton.attr("href"));
                    });
                    $("#modalDeleteConfirmation").modal();
                });
            ';
        } else {

            $jscript .= '
                $("' . $this->clickedComponent . '").on("click", function(e) {
                    e.preventDefault();
                    var aButton = $(this);
                    $("#modalDeleteConfirmation").on("show.bs.modal", function (e) {
                            var modal = $(this);
                            modal.find(".modal-title").text("Delete Data: (" + aButton.attr("' . $this->modelAttributeId . '") + ") " + aButton.attr("' . $this->modelAttributeName . '"));
                            modal.find("a#aYes").attr("href", aButton.attr("href"));
                    });
                    $("#modalDeleteConfirmation").modal();
                });
            ';
        }

        $jscript .= '
            $("#modalDeleteConfirmation").find("a#aYes").on("click", function() {

                var thisObj = $(this);

                $("#modalDeleteConfirmation").modal("hide");

                $("#modalDeleteConfirmation").on("hidden.bs.modal", function (e) {

                    $.ajax({
                        cache: false,
                        type: "POST",
                        url: thisObj.attr("href"),
                        beforeSend: function(xhr) {
                            $(".overlay").show();
                            $(".loading-img").show();
                        },
                        success: function(response) {

                            $(".overlay").hide();
                            $(".loading-img").hide();

                            $.ajax({
                                cache: false,
                                url: response.url,
                                beforeSend: function(xhr) {
                                    $(".overlay").show();
                                    $(".loading-img").show();
                                },
                                success: function(response) {

                                    $("#main-content").html(response);

                                    $(".overlay").hide();
                                    $(".loading-img").hide();
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr);

                                    $(".overlay").hide();
                                    $(".loading-img").hide();

                                    $(".error-overlay").find(".error-number").html(xhr.status);
                                    $(".error-overlay").find(".error-message").html(xhr.responseText);
                                    $(".error-overlay").show();

                                    setTimeout(function() {
                                        $(".error-overlay").hide();
                                    }, 1000);
                                }
                            });
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr);

                            $(".overlay").hide();
                            $(".loading-img").hide();

                            $(".error-overlay").find(".error-number").html(xhr.status);
                            $(".error-overlay").find(".error-message").html(xhr.responseText);
                            $(".error-overlay").show();

                            setTimeout(function() {
                                $(".error-overlay").hide();
                            }, 1000);
                        }
                    });
                });

                return false;
            });
        ';

        return $jscript;
    }

    public function theScript($index = true) {

        $this->getView()->registerJs($this->getScript($index));
    }

    public function renderDialog() {
        return $this->render('modalDialog', [

        ]);
    }
}
